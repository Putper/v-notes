/**
 * Interface for the counter state
 */
export default interface ICounterState
{
    counter: number
}
