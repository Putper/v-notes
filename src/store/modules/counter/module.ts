import store from '@/store'
import { Module, VuexModule, Mutation } from 'vuex-module-decorators'
import ICounterState from './interface'

// module
@Module({name:'counter', dynamic:true, store})
export default class Counter extends VuexModule implements ICounterState
{
    counter: number = 0
    
    @Mutation
    increaseCounter(amount:number)
    {
        this.counter += amount
    }
    
    @Mutation
    decreaseCounter(amount:number)
    {
        this.counter -= amount
    }

    // needed an excuse to test getters lmao
    get counterWithEmoji()
    {
        return this.counter + '👨‍💻'
    }
}
