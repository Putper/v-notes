import { getModule } from 'vuex-module-decorators'
import Files from './files/module'

export default getModule(Files)
