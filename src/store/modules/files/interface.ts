import IFileModel from '@/models/files/IFileModel'

export default interface IFilesState
{
    files: Array<IFileModel>
}
