import store from '@/store'
import { Module, VuexModule, MutationAction, Mutation, Action } from 'vuex-module-decorators'
import IFileModel from '@/models/files/IFileModel'
import DirectoryModel from '@/models/files/DirectoryModel'
import RegularFileModel from '@/models/files/RegularFileModel'
import IFilesState from './interface'

@Module({name:'files_module', dynamic:true, store})
export default class Files extends VuexModule implements IFilesState
{
    _files: Array<IFileModel> = []

    get files()
    {
        return this._files
    }

    @Mutation
    addFile(file:IFileModel)
    {
        this._files.push(file)   
    }

    @Mutation
    resetFiles()
    {
        this._files = []
    }

    @Action
    setFiles()
    {
        // only set if empty
        if(this.files.length !== 0)
            this.resetFiles()

        // generate some random files
        for(let i_folder=1; i_folder<4; ++i_folder)
        {
            // unique string for ids
            let random = Math.random().toString(36).substring(7)
            // how many files to create
            const amount = Math.floor( Math.random() * 4 + 2)

            // create files for folder
            let folder_files = []
            for(let i_file=1; i_file<amount; ++i_file)
            {
                let reg_file = new RegularFileModel(`file_${i_file}_${random}`, 'txt', `File ${i_file}`)
                folder_files.push(reg_file)
            }

            let directory = new DirectoryModel(`folder_${i_folder}_${random}`, `Folder ${i_folder}`)
                .addChildren(folder_files)

            this.addFile(directory)
        }
    }


    /**
     * find a file based on ID
     * @param id a file id
     * @param files directory to chieck
     * @return the found file, or null if not found
     */
    @Action
    findById(id:string, files?:IFileModel[]): IFileModel|null
    {
        // if no files are given, assume to check all files
        if(files===undefined)
            files = this.files

        // check each file
        files.forEach(file =>
        {
            if( file.id === id )
                return file
            
            // perform check on children, if it has any
            if(file instanceof DirectoryModel && file.children.length !== 0)
            {
                const found = this.findById(id, file.children)
                if( found!==null )
                    return found
            }
        })

        return null
    }
}
