/**
 * Here we export the counter for easier access
 */
import { getModule } from 'vuex-module-decorators'
import Counter from './counter/module'

export default getModule(Counter)
