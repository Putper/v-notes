import IFileModel from './IFileModel'

export default class FileModel implements IFileModel
{
    public id: string
    public name: string
    public is_active: boolean = false
    public type: string
    public path: string = '/'

    constructor(id:string, type:string, name:string="file")
    {
        this.type = type
        this.name = name
        this.id = encodeURI(this.fullname)
    }

    get fullname(): string
    {
        return this.name + '.' + this.type
    }

    /**
     * Get current icon
     * @return string
     */
    get icon(): string
    {
        return 'mdi-file'
    }

    /**
     * Toggle active
     * @return boolean  returns new is_active value
     */
    toggleActive(): boolean
    {
        this.is_active = !this.is_active
        return this.is_active
    }
}
