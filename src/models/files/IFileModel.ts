export default interface IFileModel
{
    /**
     * Unique ID
     */
    id: string

    /**
     * display title
     */
    name: string

    /**
     * which filetype it is
     */
    type: string

    /**
     * if its currently active
     */
    is_active: boolean

    /**
     * icon to show
     */
    icon: string
}
