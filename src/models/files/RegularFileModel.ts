import FileModel from './FileModel'
import FileExtension from '@/types/FileExtension'

export default class RegularFileModel extends FileModel
{
    get icon()
    {
        switch(this.type)
        {
            case 'txt':
                return 'mdi-note-text'
            default:
                return 'mdi-file'
        }
    }
}
