import FileModel from './FileModel'
import IFileModel from './IFileModel'

export default class DirectoryModel extends FileModel
{
    protected _children: Array<IFileModel> = []

    constructor(id:string, name:string="new folder")
    {
        super(id, 'dir', name)
    }

    /**
     * Get all children
     */
    get children(): Array<IFileModel>
    {
        return this.children
    }
    

    /**
     * Add file to children
     * @param child child to add
     */
    addChild(child:IFileModel): DirectoryModel
    {
        this._children.push(child)
        return this
    }

    /**
     * add multiple files to children
     * @param children array of children to add
     */
    addChildren(children:Array<IFileModel>): DirectoryModel
    {
        children.forEach(child =>
        {
            this.addChild(child)
        })

        return this
    }
    
    /**
     * remove given child
     * @param child child to remove
     */
    removeChild(child:IFileModel): DirectoryModel
    {
        for( let index=0; index<this._children.length; ++index)
        {
            if(child.id === this._children[index].id)
            {
                this._children.splice(index, 1)
                break
            }
        }

        return this
    }


    
    get icon()
    {
        return (this.is_active)
            ? 'mdi-folder-open'
            : 'mdi-folder'
    }
}
