import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router(
{
    mode: 'history',
    base: process.env.BASE_URL,
    routes:
    [
        {
            path: '/',
            name: 'home',
            redirect: { name: 'browser'}
        },
        {
            path: '/about',
            name: 'about',
            component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
        },
        {
            path: '/counter',
            name: 'counter',
            component: () => import(/* webpackChunkName: "counter" */ '@/views/Counter.vue')
        },
        {
            path: '/browser/:files*',
            name: 'browser',
            component:() => import(/* webpackChunkName: "browser" */ '@/views/Browser.vue')
        }
    ]
})
