import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify,
{
    theme:
    {
        primary: colors.green.base,
        secondary: colors.grey.lighten5,
        accent: colors.amber.base,
        error: colors.red.accent2,
        info: colors.blue.base,
        success: colors.green.lighten2,
        warning: colors.amber.lighten1

    },
    iconfont: 'mdi',
})
