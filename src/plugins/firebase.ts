import Vue from 'vue'
import FieryVue from 'fiery-vue'
import firebase from 'firebase'
import 'firebase/firestore'

/**
 * Initialise Firebase
 */
const app = firebase.initializeApp(
    {
        apiKey: process.env.VUE_APP_FIREBASE_API,
        authDomain: process.env.VUE_APP_FIREBASE_DOMAIN,
        databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
        projectId: process.env.VUE_APP_PROJECT_ID,
        storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
        messagingSenderId: process.env.VUE_APP_FIREBASE_SENDER_ID
    }
)


/**
 * Initialise FireStore
 */
const firestore = firebase.firestore(app)
// enable persistence for offine use
// firestore.enablePersistence()


/**
 * Initialise FieryVue
 */
Vue.use(FieryVue)


/**
 * Put things in the prototype for use in other files
 */
Vue.prototype.$firestore = firestore
Vue.prototype.$firebase = firebase
